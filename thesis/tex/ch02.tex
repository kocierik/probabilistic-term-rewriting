\chapter{La riscrittura probabilistica dei termini}

In questo capitolo, presentiamo la riscrittura probabilistica dei termini come
un processo decisionale di Markov, sulle orme di Bournez e Garnier
\cite{bournez}. Nessun risultato originale viene presentato: ci si limita a una
presentazione rigorosa che sia anche funzionale al resto dello studio. Per
aiutare la lettrice e il lettore, il caso probabilistico sarà sempre preceduto
da quello non-probabilistico, anche se quest'ultimo non è necessario ai nostri
scopi.

\section{Sistemi di riduzione astratti probabilistici}

Presupposto per parlare di riscrittura dei termini è aver chiaro il concetto di
sistemi di riduzione astratti, che prescinde dallo studio dei termini lavorando
su oggetti arbitrari.

\begin{definition}[Sistema di riduzione astratto]
  Sia $A$ un insieme. $\rightarrow$ è un sistema di riduzione astratto su $A$ se
  e solo se
  $$\rightarrow \subseteq A^2$$
\end{definition}

Quando passiamo allo scenario probabilistico, usiamo le multidistribuzioni.
Facciamo inoltre uso dell'abbreviazione PARS (\emph{Probabilistic Abstract
Reduction System}).

\begin{definition}[Sistema di riduzione astratto probabilistico]
  Sia $A$ un insieme. $\rightarrow$ è un sistema di riduzione astratto
  probabilistica su $A$ se e solo se
  $$\rightarrow \subseteq A \times \mathcal{M}(A)$$
\end{definition}

I dati che non possono essere ridotti prendono il nome di ``forme normali''. La
loro definizione resta sostanzialmente inalterata nel caso probabilistico.

\begin{definition}[Forma normale di un sistema di riduzione astratto]
  Siano $\rightarrow$ un sistema di riduzione astratto su $A$ e $a \in A$. Si
  dice che $a$ è una forma normale in $\rightarrow$ quando
  $$\nexists b \in A \quad a \rightarrow b$$
\end{definition}

\begin{definition}[Forma normale di un sistema di riduzione astratto
  probabilistico]
  Siano $\rightarrow$ un sistema di riduzione astratto probabilistico su $A$ e
  $a \in A$. Si dice che $a$ è una forma normale in $\rightarrow$ quando
  $$\nexists \mu \in \mathcal{M}(A) \quad a \rightarrow \mu$$
\end{definition}

Le forme normali rappresentano il risultato finale di un processo di riduzione,
e il loro insieme è quindi un oggetto di rilievo.

\begin{definition}[Insieme delle forme normali di un sistema di riduzione
  astratto]
  Sia $\rightarrow$ un sistema di riduzione astratto su $A$. L'insieme delle
  forme normali di $\rightarrow$, $NF_\rightarrow$, è definito come
  $$NF_\rightarrow := \{ a \in A \mathbin{|} \nexists b \in A \quad a \rightarrow b \}$$
\end{definition}

\begin{definition}[Insieme delle forme normali di un sistema di riduzione
  astratto probabilistico]
  Sia $\rightarrow$ un sistema di riduzione astratto probabilistico su $A$.
  L'insieme delle forme normali di $\rightarrow$, $NF_\rightarrow$, è definito
  come
  $$NF_\rightarrow := \{ a \in A \mathbin{|} \nexists \mu \in \mathcal{M}(A) \quad a \rightarrow \mu \}$$
\end{definition}

A partire da un PARS su $A$, possiamo costruire un ARS che espliciti il
comportamento del primo oggetto su un'arbitraria multidistribuzione su $A$. In
virtù del \cref{cor:multidistributions-are-closed-under-subconvex-conbinations},
la seguente definizione riguarda davvero un ARS su $\mathcal{M}_{\leq 1}(A)$. 

\begin{definition}[Relazione di riduzione probabilistica]\label{def:probabilistic-reduction-relation}
  Sia $\rightarrow$ un PARS su $A$. La relazione di riduzione probabilistica di
  $\rightarrow$, $\xrightarrow{\mathcal{M}}$, è il più piccolo ARS su
  $\mathcal{M}_{\leq 1}(A)$ che rispetta le seguenti regole:
  \begin{enumerate}
    \item $\forall a \in NF_{\rightarrow} \quad \{\!\!\{1 : a\}\!\!\} \xrightarrow{\mathcal{M}} \varnothing$;
    \item $\forall (a, \mu) \in \rightarrow \quad \{\!\!\{1 : a\}\!\!\} \xrightarrow{\mathcal{M}} \mu$; 
    \item se $I$ è un insieme finito tale che
      $\sum_{i \in I} p_i \leq 1 \wedge \forall i \in I (p_i \geq 0 \wedge \mu_i \xrightarrow{\mathcal{M}} \nu_i)$,
      allora $\biguplus_{i \in I} p_i \cdot \mu_i \xrightarrow{\mathcal{M}} \biguplus_{i \in I} p_i \cdot \nu_i$.
  \end{enumerate}
\end{definition}

Le relazioni di riduzione probabilistiche ci permettono di modellare l'evolversi
di un processo di riduzione, che identifichiamo con una sequenza di riduzione
probabilistica.

\begin{definition}[Sequenza di riduzione]
  Siano $\rightarrow$ un PARS su $A$ e $a \in A$. Una sequenza $(a_i)_{i \in
  \{1, \dots, n\}}$ con $n \in \mathbb{N}_{>0}$ è una sequenza di riduzione in
  $\rightarrow$ da $a$ se e solo se:
  $$a_1 = a \wedge a_n \in NF_{\rightarrow} \wedge \forall i \in \{2, \dots, n\}
  \quad a_i \rightarrow a_{i + 1}$$
\end{definition}

\begin{definition}[Sequenza di riduzione probabilistica da una
  multidistribuzione]
  Siano $\rightarrow$ un PARS su $A$ e $\mu \in \mathcal{M}_{\leq 1}(A)$.
  Una successione $(\mu_i)_{n \in \mathbb{N}}$ è una sequenza di riduzione
  probabilistica in $\rightarrow$ da $\mu$ se e solo se:
  $$\mu_0 = \mu \wedge \forall n \in \mathbb{N} \quad \mu_n \xrightarrow{\mathcal{M}} \mu_{n + 1}$$
\end{definition}

Ci tornerà spesso utile fare riferimento all'insieme di tutte le possibili
sequenze di riduzioni probabilistiche da una data multidistribuzione.

\begin{definition}[Insieme delle sequenze di riduzione]
  Siano $\rightarrow$ un ARS su $A$ e $a \in A$. L'insieme delle sequenze di
  riduzione in $\rightarrow$ da $a$, $red_{\rightarrow}(a)$, è definito come
  $$red_{\rightarrow}(a) :=
  \{ S \mathbin{|} \text{$S$ è una sequenza di riduzione in $\rightarrow$ da
  $a$}\}$$
\end{definition}

\begin{definition}[Insieme delle sequenze di riduzione probabilistica da una
  multidistribuzione]
  Siano $\rightarrow$ un PARS su $A$ e $\mu \in \mathcal{M}_{\leq 1}(A)$.
  L'insieme delle sequenze di riduzione probabilistica (s.r.p.) in $\rightarrow$
  da $\mu$, $red_{\rightarrow}(\mu)$, è definito come
  $$red_{\rightarrow}(\mu) :=
  \{ S : \mathbb{N} \rightarrow \mathcal{M}_{\leq 1}(A) \mathbin{|}
  \text{$S$ è una s.r.p. in $\rightarrow$ da
  $\mu$}\}$$
\end{definition}

Per estensione, possiamo parlare di sequenze di riduzione probabilistica anche
in assenza di multidistribuzioni.

\begin{definition}[Sequenza di riduzione probabilistica da un
  valore]
  Siano $\rightarrow$ un PARS su $A$ e $a \in A$.
  Una successione $(\mu_i)_{n \in \mathbb{N}}$ è una sequenza di riduzione
  probabilistica in $\rightarrow$ da $a$ se e solo se è una sequenza di
  riduzione probabilistica in $\rightarrow$ da $\{\!\!\{1 : a\}\!\!\}$.
\end{definition}

\begin{definition}[Insieme delle sequenze di riduzione probabilistica da un
  valore]
  Siano $\rightarrow$ un PARS su $A$ e $a \in A$.
  L'insieme delle sequenze di riduzione probabilistica in $\rightarrow$ da
  $a$, $red_{\rightarrow}(a)$, è definito come
  $$red_{\rightarrow}(a) := red_{\rightarrow}(\{\!\!\{1 : a\}\!\!\})$$
\end{definition}

A breve, vedremo nelle sequenze di riduzione il soggetto adatto per il nostro
discorso riguardante la terminazione.

\section{Sistemi probabilistici di riscrittura dei termini}

La riscrittura dei termini introduce un proprio lessico specifico, legato
al campo semantico della (ri)scrittura stessa.

\begin{definition}[Regola di riscrittura]
  Siano $V$ un insieme di variabili tale che $|V| = \aleph_0$ e $T(\Sigma, V)$
  l'insieme dei $\Sigma$-termini su $V$. Definiamo $l \rightarrow r$, una
  generica $\Sigma$-regola di riscrittura su $V$ come
  $$l \rightarrow r := (l, r) \quad , \quad
  \begin{cases}
  (l, r) \in T^2(\Sigma, V) \\
  l \not\in V\\
  \mathcal{V}\mathit{ar}(l) \supseteq \mathcal{V}\mathit{ar}(r)
  \end{cases}$$
\end{definition}

Manteniamo il secondo e il terzo vincolo, che chiaramente permettono di evitare
casi patologici o di ovvia non-terminazione, più per compatibilità con lavori
precedenti che per una reale necessità.

Nella loro versione probabilistica, le regole di riscrittura sostituiscono il
loro membro destro con una multidistribuzione. Avremo quindi prima bisogno di
esplicitare la definizione di insieme delle variabili occorrenti in una
multidistribuzione su un insieme di termini.

\begin{definition}[Insieme delle variabili occorrenti in una multidistribuzione
su un insieme di termini]
  Siano $V$ un insieme di variabili tale che $|V| = \aleph_0$, $T(\Sigma, V)$
  l'insieme dei $\Sigma$-termini su $V$ e $\mu$ tale che
  $\mu \in \mathcal{M}_{\leq 1}(T(\Sigma, V))$. L'insieme delle variabili occorrenti in
  $\mu$ è:
  $$\mathcal{V}\mathit{ar}(\mu) := \bigcup_{s \in T(\Sigma, V) \mathbin{,} \mu(s) > 0}\mathcal{V}\mathit{ar}(s)$$ 
\end{definition}

\begin{definition}[Regola di riscrittura probabilistica]
  Siano $V$ un insieme di variabili tale che $|V| = \aleph_0$ e $T(\Sigma, V)$
  l'insieme dei $\Sigma$-termini su $V$. Definiamo $l \rightarrow \mu$, una
  generica $\Sigma$-regola di riscrittura probabilistica su $V$ come
  $$l \rightarrow \mu := (s, \mu) \quad , \quad
  \begin{cases}
  (l, \mu) \in T(\Sigma, V) \times \mathcal{M}(T(\Sigma, V))\\
  l \not\in V\\
  \mathcal{V}\mathit{ar}(l) \supseteq \mathcal{V}\mathit{ar}(\mu)
  \end{cases}$$
\end{definition}

Siamo ora in grado di passare a un concetto centrale: quello di sistema
(probabilistico o meno) di riscrittura dei termini.

\begin{definition}[Sistema di riscrittura di termini]
  Siano $V$ un insieme di variabili tale che $|V| = \aleph_0$ e $T(\Sigma, V)$
  l'insieme dei $\Sigma$-termini su $V$. Un insieme $R$ si dice essere un
  sistema di riscrittura di $\Sigma$-termini su $V$ se e solo se:
  $$\forall x \in R, \quad \text{$x$ è una $\Sigma$-regola di riscrittura su
  $V$}$$
\end{definition}

L'estensione probabilistica è banale. Per brevità, talvolta lo abbrevieremo come
PTRS (\emph{Probabilistic Term Rewriting System}).

\begin{definition}[Sistema di riscrittura di termini probabilistico]
  Siano $V$ un insieme di variabili tale che $|V| = \aleph_0$ e $T(\Sigma, V)$
  l'insieme dei $\Sigma$-termini su $V$. Un insieme $R$ si dice essere un
  sistema di riscrittura probabilistico di $\Sigma$-termini su $V$ se e solo se:
  $$\forall x \in R, \quad \text{$x$ è una $\Sigma$-regola di riscrittura
  probabilistica su $V$}$$
\end{definition}

Per esprimere il modo in cui le regola di riscrittura di un sistema di
riscrittura dei termini possano essere applicate per riscrivere un termine in un
altro, abbiamo bisogno di un'appropriata relazione di riscrittura.

\begin{definition}[Relazione di riscrittura]
  Siano $V$ un insieme di variabili tale che $|V| = \aleph_0$ e $R$ un sistema
  di riscrittura di $\Sigma$-termini su $V$. La relazione di riscrittura di $R$,
  $\rightarrow_R$, è il seguente ARS su $T(\Sigma, V)$:
  $$\rightarrow_R := \{(C[\hat{\sigma}(l)], C[\hat{\sigma}(r)]) \mathbin{|} C \in \mathcal{C}\mathit{on}(T(\Sigma, V)), \sigma \in \mathcal{S}\mathit{ub}(T(\Sigma, T)), l \rightarrow r \in R\}$$
\end{definition}

Prima di passare alla versione probabilistica, è essenziale definire come le
multidistribuzioni interagiscano con sostituzioni e contesti.

\begin{definition}[Sostituzione per multidistribuzioni]
  Siano $\hat\sigma$ una $T(\Sigma, X)$-sostituzione di variabili e $\mu$ una
  multidistribuzione su $T(\Sigma, X)$. La $T(\Sigma, X)$-sostituzione per
  multidistribuzioni basata su $\hat\sigma$, $\hat{\hat\sigma}$, è definita
  come:
  $$\hat{\hat\sigma} := \{\!\!\{p:\hat\sigma(a) \mathbin{|} p:a \in \mu\}\!\!\}$$
\end{definition}

\begin{definition}[Popolazione di un contesto per multidistribuzioni]
  Sia $C$ un contesto per $\Sigma$-termini su $V$ in cui $\Box$ occorre in
  posizione $p$ e sia $\mu$ una multidistribuzione su $T(\Sigma, V)$. La
  popolazione del contesto $C$ con $\mu$, $C[\mu]$, è definita come
  $$C[s] := \{\!\!\{p:C[a] \mathbin{|} p:a \in \mu\}\!\!\}$$
\end{definition}

Disponiamo ora di tutti gli strumenti necessari.

\begin{definition}[Relazione di riscrittura probabilistica]
  Siano $V$ un insieme di variabili tale che $|V| = \aleph_0$ e $R$ un PTRS
  per $\Sigma$-termini su $V$. La relazione di riscrittura probabilistica di
  $R$, $\rightarrow_R$, è il seguente PARS su $T(\Sigma, V)$:
  $$\rightarrow_R := \{(C[\hat\sigma(l)], C[\hat{\hat\sigma}(\mu)]) \mathbin{|} C \in \mathcal{C}\mathit{on}(T(\Sigma, V)), \sigma \in \mathcal{S}\mathit{ub}(T(\Sigma, T)), l \rightarrow \mu \in R\}$$
\end{definition}

Esplicitata la definizione di relazione di riscrittura probabilistica, abbiamo
ora modo di fare riferimento al PARS ``sotteso'' a un generico PTRS.

\section{La terminazione probabilistica}

In un contesto probabilistico, la comune nozione di terminazione cede il passo
a un carosello di opzioni via via più stringenti. Definiremo queste proprietà
inizialmente su generici PARS. Poi, le istanzieremo nel caso dei PTRS, che
diremo godere di esse quando lo stesso vale per le rispettive relazioni di
riscrittura probabilistica. Prima di fare questo, però, ci concediamo un momento
per rievocare la definizione non probabilistica di sistema di riscrittura dei
termini terminante.

\begin{definition}[Sistema di riduzione astratto terminante]
  Sia $\rightarrow$ un ARS su $A$. Si dice che $\rightarrow$ è terminante quando
  $$\nexists \{a_i\}_{i \in \mathbb{N}} \in A^\mathbb{N},
  \forall i \in \mathbb{N} a_i \rightarrow a_{i + 1}$$
\end{definition}

\begin{definition}[Sistema di riscrittura dei termini terminante]
  Sia $R$ un TRS. $R$ è terminante quando il suo ordine di riscrittura lo è.
\end{definition}

\subsection{Terminazione quasi certa}

Concluso il caso non probabilistico, partiamo con la condizione più debole,
quella di terminazione quasi certa (AST, \emph{Almost Sure Termination}), che
vuole simulare l'imposizione che le sequenze infinite di riduzione si
verifichino con proprietà nulla.

\begin{definition}[Sistema di riduzione astratto probabilistico quasi
  certamente terminante]
  Sia $\rightarrow$ un sistema di riduzione astratto probabilistico su un
  insieme $A$. $\rightarrow$ si dice essere quasi certamente terminante se e
  solo se
  $$\forall \mu \in \mathcal{M}_{\leq 1}(A),
  (\mu_n)_{n \in \mathbb{N}} \in red_{\rightarrow}(\mu) \quad
  \lim_{n\to\infty}|\mu_n| = 0$$
\end{definition}

\begin{definition}[Sistema di riscrittura dei termini probabilistico quasi
  terminante]
  Sia $R$ un PTRS. Si dice che R è quasi certamente terminante se e solo se
  la sua relazione di riscrittura probabilistica è quasi certamente terminante.
\end{definition}

\subsection{Terminazione quasi certa positiva}

L'idea di terminazione può anche essere modellata vincolando la lunghezza attesa
delle derivazioni.

\begin{definition}[Lunghezza di derivazione di una sequenza di riduzione]
  Siano $\rightarrow$ un ARS su $A$, $a \in A$ e $(a_i)_{i \in \{1, \dots, n\}}
  \in red_{\rightarrow}(a)$. La lunghezza della sequenza di riduzione
  probabilistica $(a_i)_{i \in \{1, \dots, n\}}$ rispetto all'ARS $\rightarrow$,
  $edl_\rightarrow((a_i)_{i \in \{1, \dots, n\}})$, è definita come
  $$dl_\rightarrow((a_i)_{i \in \{1, \dots, n\}}) := n$$
\end{definition}

\begin{definition}[Lunghezza di derivazione attesa di una sequenza di riduzione
  probabilistica]
  Siano $\rightarrow$ un PARS su $A$, $\mu \in \mathcal{M}_{\leq 1}(A)$ e
  $\vec{\mu} \in red_{\rightarrow}(\mu)$. La lunghezza attesa
  della sequenza di riduzione probabilistica $\vec{\mu}$ rispetto al PARS
  $\rightarrow$, $edl_\rightarrow(\vec{\mu})$, è definita come
  $$edl_\rightarrow(\vec{\mu}) := \sum_{n \in \mathbb{N}}|\mu_n|$$
\end{definition}

In particolare, il vincolo che ci interessa in un'ottica di terminazione è
quello di finitezza, che vorremmo imporre su tutte le possibili sequenze di
riduzione. Questa accezione di terminazione probabilistica è detta
terminazione quasi certa positiva (PAST, \emph{Positive Almost Sure
Termination}).

\begin{definition}[Sistema di riduzione astratto probabilistico quasi
  certamente terminante positivamente]
  Sia $\rightarrow$ un sistema di riduzione astratto probabilistico su un
  insieme $A$. $\rightarrow$ si dice essere quasi certamente terminante
  positivamente se e solo se
  $$\forall \mu \in \mathcal{M}_{\leq 1}(A),
  \vec{\mu} \in red_{\rightarrow}(\mu) \quad
  \text{$edl_{\rightarrow}(\vec{\mu})$ è finito}$$
\end{definition}

\begin{definition}[Sistema di riscrittura dei termini probabilistico quasi
  certamente terminante positivamente]
  Sia $R$ un PTRS. Si dice che R è quasi certamente terminante positivamente se
  e solo se la sua relazione di riscrittura probabilistica è quasi certamente
  terminante positivamente.
\end{definition}

Come già anticipato, PAST è una proprietà più forte di AST.

\begin{theorem}[PAST implica AST]
  Sia $R$ un PTRS. Se $R$ è PAST, allora $R$ è AST.
\end{theorem}

\begin{proof}
  Se $R$ è PAST, allora lo è anche la sua relazione di riscrittura
  probabilistica, che chiamiamo $\rightarrow$. Di conseguenza, una sua generica
  sequenza di riduzione avrà come serie delle probabilità totali dei suoi
  termini un valore finito. Quindi, la successione dei termini in questione
  converge a 0. Perciò $\rightarrow$ è AST, e pertanto anche $R$ deve esserlo.
\end{proof}

\subsection{Terminazione quasi certa forte}

Un sistema di riscrittura dei termini PAST ci dà garanzia che tutte le
sequenze di riduzione della sua relazione di riscrittura abbiano una lunghezza
attesa sì finita, ma potenzialmente arbitrariamente grande. Ci interessa quindi
definire un limite superiore per le lunghezze di derivazione (che chiameremo
``altezza di derivazione'') a partire da una data multidistribuzione iniziale.

\begin{definition}[Altezza di derivazione]
  Siano $\rightarrow$ un ARS su $A$ e $a \in A$. L'altezza di derivazione attesa
  di $a$ rispetto all'ARS $\rightarrow$, $dh_\rightarrow(a)$, è definita come
  $$dh_\rightarrow(a) := \sup\{dl_\rightarrow(\vec{a}) \mathbin{|}
  \vec{a} \in red_\rightarrow(a)\}$$
\end{definition}

\begin{definition}[Altezza di derivazione attesa di una multidistribuzione]
  Siano $\rightarrow$ un PARS su $A$ e $\mu \in \mathcal{M}_{\leq 1}(A)$. L'altezza di
  derivazione attesa di $\mu$ rispetto al PARS $\rightarrow$,
  $edh_\rightarrow(\mu)$, è definita come
  $$edh_\rightarrow(\mu) := \sup\{edl_\rightarrow(\vec{\mu}) \mathbin{|}
  \vec{\mu} \in red_\rightarrow(\mu)\}$$
\end{definition}

Non pretendiamo quindi che esista un unico vincolo superiore alle lunghezze di
derivazione attese comune a tutte le multidistribuzioni iniziali, ma solo che,
per ogni distribuzione degenere che assegna a un valore iniziale probabilità
1, disponiamo di un vincolo locale di questo tipo. Necessitiamo quindi di
estendere la nostra definizione di altezza di derivazione attesa anche agli
elementi dell'insieme iniziale.

\begin{definition}[Altezza di derivazione attesa di un valore]
  Siano $\rightarrow$ un PARS su $A$ e $a \in A$. L'altezza di derivazione
  attesa di $a$ rispetto al PARS $\rightarrow$, $edh_\rightarrow(\mu)$, è
  definita come
  $$edh_\rightarrow(a) := edh_\rightarrow(\{\!\!\{1 : a\}\!\!\})$$
\end{definition}

Alla proprietà in questione daremo il nome di ``terminazione quasi certa forte''
(SAST, \emph{Strong Almost Sure Termination}).

\begin{definition}[Sistema di riduzione astratto probabilistico quasi
  certamente terminante forte]
  Sia $\rightarrow$ un sistema di riduzione astratto probabilistico su un
  insieme $A$. $\rightarrow$ si dice essere quasi certamente terminante
  forte se e solo se
  $$\forall a \in A \quad \text{$edh_{\rightarrow}(a)$ è finito}$$
\end{definition}

\begin{definition}[Sistema di riscrittura dei termini probabilistico quasi
  certamente terminante forte]\label{def:past-ptrs}
  Sia $R$ un PTRS. Si dice che R è quasi certamente terminante forte se e solo
  se la sua relazione di riscrittura probabilistica è quasi certamente
  terminante forte.
\end{definition}

In questo caso, SAST è più stringente di PAST. Per provarlo, necessitiamo di
introdurre qualche lemma.

\begin{lemma}[Scomposizione di una sequenza di riduzione]\label{lem:decomposition-of-reduction-sequences}
  Siano $\rightarrow$ un PARS su $A$ e
  $\vec{\mu} \in \mathcal{M}_{\leq 1}^\mathbb{N}(A)$. Allora:
  $$\vec{\mu} \in red_\rightarrow(\biguplus_{i \in I}p_i \cdot \nu_i)$$
  se e solo se:
  $$\exists f: I \rightarrow \mathcal{M}_{\leq 1}^\mathbb{N}(A),
  (\vec{\mu} = \biguplus_{i \in I} p_i \cdot f(i)
  \wedge \forall i \in I, f(i) \in red_\rightarrow(\nu_i))$$
\end{lemma}

\begin{proof}
  Da sinistra a destra: Costruisco una procedura che, preso un valore per
  $i \in I$ costruisce $\vec{\rho_i} = f(i)$ per induzione su $n \in \mathbb{N}$,
  indice del termine corrente nella successione di multidistribuzioni.
  \begin{itemize}
    \item caso base: costruisco i termini di indice $0$ in modo tale che
      $\forall i \in I,\rho_{i,0} = \nu_i$;
    \item caso induttivo: assumo, per ipotesi induttiva, che
      $\mu_n = \biguplus_{i \in I} p_i \cdot \rho_{i,n}$. Costruisco i termini
      di indice $n + 1$ in modo tale che
      $\forall i \in I, \rho_{i,n} \xrightarrow{\mathcal{M}} \rho_{i,n+1}$.
      Quindi, per la \cref{def:probabilistic-reduction-relation}, segue
      $\biguplus_{i \in I} p_i \cdot \rho_{i,n} \xrightarrow{\mathcal{M}} \biguplus_{i \in I} p_i \cdot \rho_{i,n+1}$,
      e cioè $\mu_n \xrightarrow{\mathcal{M}} \mu_{n+1}$.
  \end{itemize}
  Da destra a sinistra: ovvio per la scelta delle regole nella
  \cref{def:probabilistic-reduction-relation}.
\end{proof}

\begin{lemma}[Formula dell'altezza di derivazione attesa]\label{lem:hdl_formula}
  Siano $\rightarrow$ un PARS su $A$ e $\biguplus_{i \in I}p_i \cdot \mu_i) \in
  \mathcal{M}_{\leq 1}(A)$. Allora:
  $$edh_\rightarrow(\biguplus_{i \in I} p_i \cdot \mu_i) =
  \sum_{i \in I}p_i \cdot edh_\rightarrow(\mu_i)$$
\end{lemma}

\begin{proof}
  $$edh_\rightarrow(\biguplus_{i \in I} p_i \cdot \mu_i)
  =\sup\{edl_\rightarrow(\vec{\nu}) \mathbin{|} \vec{\nu} \in red_\rightarrow(\biguplus_{i \in I}p_i \cdot \mu_i)\}=$$
  $$=\sup\{\sum_{n \in \mathbb{N}}|\nu_n| \mathbin{|} \vec{\nu} \in red_\rightarrow(\biguplus_{i \in I}p_i \cdot \mu_i)\}=$$
  (Per il \cref{lem:decomposition-of-reduction-sequences}:)
  $$=\sup\{\sum_{n \in \mathbb{N}}|\biguplus_{i \in I} p_i \cdot \nu_{n,i}| \mathbin{|} \forall i \in I (\nu_{n,i})_{n \in \mathbb{N}} \in red_\rightarrow(\mu_i)\}=$$
  (Per il \cref{cor:multidistributions-are-closed-under-subconvex-conbinations}:)
  $$=\sup\{\sum_{i \in I} p_i \cdot \sum_{n \in \mathbb{N}} |\nu_{n,i}| \mathbin{|} \forall i \in I (\nu_{n,i})_{n \in \mathbb{N}} \in red_\rightarrow(\mu_i)\}=$$
  $$= \sum_{i \in I}p_i \cdot \sup\{\sum_{n \in \mathbb{N}}|\nu_n| \mathbin{|} (\nu_n)_{n \in \mathbb{N}} \in red_\rightarrow(\mu_i)\}=$$
  $$= \sum_{i \in I}p_i \cdot edh_\rightarrow(\mu_i)$$
\end{proof}

\begin{theorem}[SAST implica PAST]
  Sia $R$ un PTRS. Se $R$ è SAST, allora $R$ è PAST.
\end{theorem}

\begin{proof}
  Se $R$ è SAST, allora lo è anche la sua relazione di riscrittura
  probabilistica, che chiamiamo $\rightarrow$. Sia $\rightarrow$ un PARS su $A$.
  Allora, per ogni $a \in A$, $edh_\rightarrow(a)$ (e cioè
  $edh_\rightarrow(\{\!\!\{1 : a\}\!\!\})$) è finito. Quindi, per il
  \cref{lem:hdl_formula}, anche l'altezza di
  derivazione attesa di una generica multidistribuzione finita su $A$ è finita.
  Di conseguenza, lo è anche la lunghezza di derivazione attesa, da essa
  superiormente limitata. Perciò $\rightarrow$ è PAST, e pertanto anche $R$ deve
  esserlo.
\end{proof}

Per provare questo teorema, abbiamo dovuto introdurre il concetto di altezza di
derivazione attesa. Una sua particolerità è che essa coincide con il proprio
valore atteso.

\begin{theorem}[Altezza di derivazione attesa e suo valore atteso]\label{lem:edh-and-expected-value}
  Siano $\rightarrow$ un PARS su $A$ e $\mu \in \mathcal{M}_{\leq 1}(A)$.
  Allora:
  $$edh_\rightarrow(\mu) = \mathbb{E}(edh_\rightarrow(\mu))$$
\end{theorem}

\begin{proof}
  Segue dal \cref{lem:hdl_formula} considerando
  $$edh_\rightarrow(\mu) = edh_\rightarrow(\biguplus_{p:a \in \mu} p \cdot \{\!\!\{1:a\}\!\!\})$$
  e osservando che quindi
  $$\sum_{p:a \in \mu} p \cdot edh_\rightarrow(\{\!\!\{1 : a\}\!\!\}) = \mathbb{E}(edh_\rightarrow(\mu))$$
\end{proof}

\section{Funzioni di rango probabilistiche}

Già a partire dal caso non probabilistico, lo strumento storicamente più diffuso
per provare la terminazione è la riduzione tramite codifica a sistemi già
riconosciuti come terminanti, come $>$ su $\mathbb{N}$.

\begin{definition}[Codifica fra sistemi di riduzione astratti]
  Siano $\rightarrow$ un ARS su $A$, $\sqsupset$ un ARS su $B$ e
  $f : A \rightarrow B$. Si dice che $f$ è una codifica di $\rightarrow$ in
  $\sqsupset$ se e solo se:
  $$\forall (a_1, a_2) \in \rightarrow, f(a_1) \sqsupset f(a_2)$$
\end{definition}

Dati due sistemi di riduzione astratti, potrebbero esistere più codifiche
corrette del primo nel secondo: usiamo il termine ``codificabile'' quando ne
esiste almeno una.

\begin{definition}[Sistema di riduzione astratto codificabile]
  Siano $\rightarrow$ e $\sqsupset$ due ARS. Si dice che $\rightarrow$ è
  codificabile in $\sqsupset$ quando esiste almeno una codifica di $\rightarrow$
  in $\sqsupset$.
\end{definition}

L'essere codificabile in almeno un ARS terminante è in effetti condizione sia
sufficiente che necessaria per la terminazione.

\begin{theorem}[Correttezza e completezza del metodo di codifica fra ARS per
  provare la terminazione]
  Sia $\rightarrow$ un ARS. $\rightarrow$ è terminante se e solo se è
  codificabile in almeno un ARS terminante.
\end{theorem}

\begin{proof}
  Da sinistra a destra: ovvio, siccome la funzione identità codifica
  $\rightarrow$ in sé stesso.
  Da destra a sinistra: assumiamo che esista una catena discendente infinita e
  ne deriviamo l'assurdo. Se infatti essa esistesse, potremmo mapparla tramite
  la codifica nell'ARS ipotizzato terminante e otterremo una catena discendente
  infinita in quest'ultimo, e questo è in contraddizione con il suo essere
  terminante.
\end{proof}

La definizione di codifica non cambia di molto quando si vuole passare allo
scenario probabilistico, se ricordiamo che possiamo sollevare funzioni rispetto
alle multidistribuzioni grazie alla
\cref{def:lifting-a-function-to-multidistribution}.

\begin{definition}[Codifica fra sistemi di riduzione astratti probabilistici]
  Siano $\rightarrow$ un PARS su $A$, $\sqsupset$ un PARS su $B$ e
  $f : A \rightarrow B$. Si dice che $f$ è una codifica di $\rightarrow$ in
  $\sqsupset$ se e solo se:
  $$\forall (a, \mu) \in \rightarrow, f(a) \sqsupset f(\mu)$$
\end{definition}

Prima di passare a dimostrare correttezza e completezza anche di quest'ultimo
metodo di dimostrazione della terminazione, necessitiamo di qualche risultato
intermedio.

\begin{lemma}\label{lem:encoding-1}
  Siano $\rightarrow$ e $\sqsupset$ due PARS e $f: A \rightarrow B$ una codifica
  di $\rightarrow$ in $\sqsupset$. Allora:
  $$\forall (\mu, \nu) \in \xrightarrow{\mathcal{M}},
  \exists \xi \in \mathcal{M}_{\leq 1}(B),
  f(\mu) \sqsupset^{\mathcal{M}} \nu \uplus \xi$$
\end{lemma}

\begin{proof}
  Per induzione sulla struttura della derivazione di
  $\mu \xrightarrow{\mathcal{M}} \nu$:
  \begin{itemize}
    \item primo caso base: $\{\!\!\{1 : a\}\!\!\} \xrightarrow{\mathcal{M}} \varnothing$,
      con $a \in NF_{\rightarrow}$.

      Dobbiamo dimostrare $\exists \xi \in \mathcal{M}_{\leq 1}(B),
      \{\!\!\{ 1 : f(a) \}\!\!\} \sqsupset^{\mathcal{M}} \xi$, che è ovvio per
      la scelta delle nostre regole di derivazione;
    \item secondo caso base: $\{\!\!\{1 : a\}\!\!\} \xrightarrow{\mathcal{M}} \mu$,
      con $(a, \mu) \in \rightarrow$.

      Dobbiamo dimostrare $\exists \xi \in \mathcal{M}_{\leq 1}(B),
      \{\!\!\{ 1 : f(a) \}\!\!\} \sqsupset^{\mathcal{M}} f(\mu) \uplus \xi$. Se scegliamo
      $\varnothing$ come $\xi$, questo è ovvio per la definizione di codifica;
    \item caso induttivo: $\biguplus_{i \in I} p_i \cdot \mu_i \xrightarrow{\mathcal{M}} \biguplus_{i \in I} p_i \cdot \nu_i$,
      con $I$ insieme finito, $\sum_{i \in I} p_i \leq 1$ e $\forall i \in I (p_i \geq 0 \wedge \mu_i \xrightarrow{\mathcal{M}} \nu_i)$.
      
      Dobbiamo dimostrare $\exists \xi \in \mathcal{M}_{\leq 1}(B),
      \biguplus_{i \in I} p_i \cdot f(\mu_i) \sqsupset^{\mathcal{M}}
      (\biguplus_{i \in I} p_i \cdot f(\nu)) \uplus \xi$, che segue
      dall'applicazione dell'ipotesi induttiva su
      $\mu_i \xrightarrow{\mathcal{M}} \nu_i$ per ogni $i \in I$. In questo caso
      scegliamo come valore di $\xi$ l'unione di tutti i valori che questa
      variabile assume nelle varie applicazioni dell'ipotesi induttiva.
  \end{itemize}
\end{proof}

Il prossimo lemma ci permette di ``trasportare'' sequenze di riduzione da un
PARS a un altro usando opportunamente una codifica data.

\begin{lemma}\label{lem:encoding-2}
  Siano $\rightarrow$ e $\sqsupset$ due PARS e $f: A \rightarrow B$ una codifica
  di $\rightarrow$ in $\sqsupset$. Allora:
  $$\forall \mu \in \mathcal{M}_{\leq 1}(A) \quad
  \forall (\mu_n)_{n \in \mathbb{N}} \in red_\rightarrow(\mu) \quad
  \exists (\nu_n)_{n \in \mathbb{N}} \in red_\sqsupset(f(\mu)),$$
  $$(edl((\mu_n)_{n \in \mathbb{N}}) \leq
  edl((\nu_n)_{n \in \mathbb{N}}) \wedge
  \forall n \in \mathbb{N} \quad f(\mu_n) \subseteq \nu_n)$$
\end{lemma}

\begin{proof}
  Costruiamo $(\nu_n)_{n \in \mathbb{N}}$ induttivamente sulla struttura di
  $(\mu_n)_{n \in \mathbb{N}}$:
  \begin{itemize}
    \item passo base: $\nu_0 = f(\mu_0)$;
    \item passo induttivo: per ipotesi induttiva, possiamo assumere
      $f(\mu_n) \subseteq \nu_n$, e cioè
      $$\exists \xi \in \mathcal{M}_{\leq 1}(B), \nu_n = f(\mu_n) \uplus \xi$$
      Inoltre, per il \cref{lem:encoding-1}:
      $$\exists \rho \in \mathcal{M}_{\leq 1}(B), f(\mu_n) \sqsupset f(\mu_{n+1}) \uplus \rho$$
      Sia $\xi' \in \mathcal{M}_{\leq 1}(M)$ scelto a piacere in modo che
      $\xi \sqsupset \xi'$. Costruiamo l'elemento corrente della successione
      come $\nu_{n+1} = f(\mu_{n+1}) \uplus \rho \uplus \xi'$. Segue che
      $\nu_n \sqsupset \nu_{n+1}$.
  \end{itemize}
  Osserviamo infine che, siccome per costruzione vale
  $$\forall n \in \mathbb{N}, |\mu_n| \leq |\nu_n|$$
  allora:
  $$edl_\rightarrow((\mu_n)_{n \in \mathbb{N}})
  \leq edl_\sqsupset((\nu_n)_{n \in \mathbb{N}})$$
  e questo conclude la prova.
\end{proof}

Questo era l'ultimo lemma necessario alla dimostrazione del teorema sopra
citato, che esplicita il valore delle codifiche come strumento per dimostrare la
terminazione di PARS.

\begin{theorem}[Correttezza e completezza delle codifiche per provare SAST]
  Un PARS $\rightarrow$ su $A$ è SAST se e solo se è codificabile in un qualche
  PARS SAST $\sqsupset$. Inoltre, in questo caso, vale:
  $$\forall a \in A, edh_\rightarrow(a) \leq edh_\sqsupset(f(a))$$
\end{theorem}

\begin{proof}
  Da sinistra a destra: ovvio, siccome la funzione identità codifica
  $\rightarrow$ in sé stesso.
  Da destra a sinistra: assumiamo di avere una codifica di $\rightarrow$ in un
  qualche PARS SAST $\sqsupset$ e dimostriamo che $\rightarrow$ sia SAST
  considerando un generico $a \in A$. Per il \cref{lem:encoding-2}, possiamo
  asserire che
  $$\sup\{edl_\rightarrow(\vec{\mu}) \mathbin{|} \vec{\mu} \in red_\rightarrow(a)\} \leq
  \sup\{edl_\sqsupset(\vec{\nu}) \mathbin{|} \vec{\nu} \in red_\sqsupset(f(a))\}$$
  e quindi $edh_\rightarrow(a) \leq edh_\sqsupset(f(a))$. Perciò se $\sqsupset$
  è SAST, lo stesso deve valere per $\rightarrow$.
\end{proof}

Di norma, si tenta di codificare verso semplici (P)ARS sull'insieme dei numeri
reali non negativi.

\begin{definition}[$\geq \epsilon +$]
  Sia $\epsilon \in \mathbb{R}^+$. Definiamo $[\geq \epsilon +]$, l'ARS
  su $\mathbb{R}_{\geq 0}$:
  $$[\geq \epsilon +] := \{ (a, b) \in \mathbb{R}_{\geq 0}^2 \mathbin{|}
  a \geq \epsilon + b\}$$
\end{definition}

Per lo scenario probabilistico, lavoriamo invece con i valori attesi.

\begin{definition}[$\geq \epsilon + \mathbb{E}$]
  Sia $\epsilon \in \mathbb{R}^+$. Definiamo $[\geq \epsilon + \mathbb{E}]$, il
  PARS su $\mathbb{R}_{\geq 0}$:
  $$[\geq \epsilon + \mathbb{E}] := \{ (a, \mu) \in \mathbb{R}_{\geq 0} \times
  \mathcal{M}(\mathbb{R}_{\geq 0}) \mathbin{|}
  a \geq \epsilon + \mathbb{E}(\mu) \}$$
\end{definition}

Ecco quindi il nostro strumento principe per provare SAST: le funzioni di rango
probabilistiche, anche note come ``funzioni di rango di Lyapunov''.

\begin{definition}[Funzione di rango probabilistica]
  Sia $\rightarrow$ un PARS. Si dice che $f$ è una funzione di rango
  probabilistica per $\rightarrow$ quando $f$ è una codifica di $\rightarrow$ in
  $[\geq \epsilon + \mathbb{E}]$.
\end{definition}

L'idea di una funzione di rango è quella di fungere da ``funzione misura'' dei
passi di computazione attesi rimanenti prima della terminazione. Rimangono da
provare correttezza e completezza di questa strategia.

\subsection{Correttezza}

Cominciamo osservando quali garanzie ci offra un singolo passo di riduzione
fondato su $[\geq \epsilon + \mathbb{E}]$.

\begin{lemma}\label{lem:probabilistic-ranking-functions-1}
  Siano $\mu, \nu \in \mathcal{M}_{\leq 1}(\mathbb{R}_{\geq 0})$. Se
  $\mu [\geq \epsilon + \mathbb{E}]^\mathcal{M} \nu$, allora
  $\mathbb{E}(\mu) \geq \epsilon \cdot |\nu| + \mathbb{E}(\nu)$.
\end{lemma}

\begin{proof}
  Per induzione sulla struttura della derivazione di
  $\mu [\geq \epsilon + \mathbb{E}]^\mathcal{M} \nu$:
  \begin{itemize}
    \item primo caso base:
      $\{\!\!\{1 : a \}\!\!\} [\geq \epsilon + \mathbb{E}]^\mathcal{M} \varnothing$,
      con $a < \epsilon$. Dobbiamo dimostrare $a \geq 0$, che è ovvio siccome
      $a \in \mathbb{R}_{\geq 0}$ per ipotesi;
    \item secondo caso base:
      $\{\!\!\{1 : a \}\!\!\} [\geq \epsilon + \mathbb{E}]^\mathcal{M} \nu$,
      con $a \geq \epsilon + \mathbb{E}(\nu)$ e
      $\nu \in \mathcal{M}(\mathbb{R}_{\geq 0})$. In questo caso, quindi,
      dobbiamo dimostrare proprio $a \geq \epsilon + \mathbb{E}(\nu)$, che è
      ovvio per ipotesi;
    \item caso induttivo:
      $\biguplus_{i \in I}p_i \cdot \mu_i [\geq \epsilon + \mathbb{E}]^\mathcal{M}
      \biguplus_{i \in I}p_i \cdot \nu_i$, con $\forall i \in I,\mu_i[\geq \epsilon + \mathbb{E}]^\mathcal{M} \nu_i$.
      Per ipotesi induttiva, possiamo assumere $\forall i \in I,\mathbb{E}(\mu_i) \geq \epsilon \cdot |\nu_i| + \mathbb{E}(\nu_i)$.
      Moltiplicando ogni disuguaglianza per $p_i$ e sommandole in seguito membro
      a membro, otteniamo
      $\sum_{i \in I} p_i \cdot \mathbb{E}(\mu_i) \geq \sum_{i \in I} p_i \cdot (\epsilon \cdot |\nu_i| + \mathbb{E}(\nu_i))$,
      e quindi $\mathbb{E}(\mu) \geq \epsilon \cdot |\nu| + \mathbb{E}(\nu)$.
  \end{itemize}
\end{proof}

\begin{lemma}\label{lem:probabilistic-ranking-functions-2}
  Sia $A$ un insieme. Allora:
  $$\forall \mu_0 \in \mathcal{M}_{\leq 1}(A),
  \vec{\mu} \in red_{[\geq \epsilon + \mathbb{E}]}(\mu_0),
  \mathbb{E}(\mu_0) \geq \epsilon \cdot edl(\vec{\mu})$$
\end{lemma}

\begin{proof}
  Partiamo dimostrando $\forall n,m \in \mathbb{N} (n \geq m \rightarrow
  \mathbb{E}(\mu_m) \epsilon \cdot \sum_{i=m+1}^n|\mu_i|)$. Procediamo per
  induzione su $n - m$:
  \begin{itemize}
    \item caso base: ci riduciamo a dimostrare $\mathbb{E}(\mu_m) \geq 0$, che è
      ovvio;
    \item caso induttivo: per il \cref{lem:probabilistic-ranking-functions-1} e
      l'ipotesi induttiva, abbiamo
      $$\mathbb{E}(\mu_m) \geq \epsilon \cdot |\mu_{m+1}| + \mathbb{E}(\mu_{m+1})
      \geq  \epsilon \cdot |\mu_{m+1}| + \epsilon \cdot \sum_{i=m+2}^n |\mu_i|
      = \epsilon \cdot \sum_{i=m+1}^n |\mu_i|$$
    \end{itemize}
    Nel caso particolare con $m = 0$, si ha
    $$\forall n \in \mathbb{N}, \mathbb{E}(\mu_m) \geq \epsilon \cdot \sum_{i=1}^n |\mu_i|$$
    che, essendo una proprietà definitivamente applicabile per $n$
    arbitrariamente grandi, è valida anche per il limite della successione in
    questione.
\end{proof}

\begin{theorem}[Correttezza delle funzioni di rango probabilistiche per SAST]
  Se $\epsilon \in \mathbb{R}^+$, allora
  $$\forall a \in \mathbb{R}_{\geq 0}, edh_{[\geq \epsilon + \mathbb{E}]}(a) \leq \frac{a}{\epsilon}$$
  (e quindi $[\geq \epsilon + \mathbb{E}]$ è SAST).
\end{theorem}

\begin{proof}
  Per il \cref{lem:probabilistic-ranking-functions-2}, vale:
  $$\forall \vec{\mu} \in red_{[\geq \epsilon + \mathbb{E}]}(a),
  edl_{[\geq \epsilon + \mathbb{E}]}(\vec{\mu}) \leq \frac{a}{\epsilon}$$
  e quindi
  $$edh_{[\geq \epsilon + \mathbb{E}]}(a) \leq \frac{a}{\epsilon}$$
\end{proof}

\subsection{Completezza}

La dimostrazione di completezza è invece più semplice, e non richiede lemmi
preliminari.

\begin{theorem}[Completezza delle funzioni di rango probabilistiche per SAST]\label{th:lyapunov-completeness}
  Se $\rightarrow$ è un PARS SAST, esiste almeno una funzione di rango
  probabilistica per $\rightarrow$.
\end{theorem}

\begin{proof}
  Sia $\rightarrow$ un PARS SAST su un generico insieme $A$. Siccome
  $\rightarrow$ è SAST, $edh_\rightarrow: A \rightarrow \mathbb{R}_{\geq 0}$.
  Se $\rightarrow = \varnothing$, il teorema è banalmente soddisfatto dalla
  codifica vuota. In caso contrario, deve esistere almeno una coppia
  $a \rightarrow \mu$, con $|\mu| = 1$. Osserviamo che:
  $$edh_\rightarrow(a) = \sup\{edl_\rightarrow(\vec{\mu}) \mathbin{|} \vec{\mu} \in red_\rightarrow(a)\}
  \geq \sup\{|\mu| + edl_\rightarrow(\vec{\mu}) \mathbin{|} \vec{\mu} \in red_\rightarrow(\mu)\} =$$
  $$= |\mu| + \sup\{edl_\rightarrow(\vec{\mu}) \mathbin{|} \vec{\mu} \in red_\rightarrow(\mu)\}
  = |\mu| + edh_\rightarrow(\mu) =$$
  (Per il \cref{lem:edh-and-expected-value}:)
  $$=|\mu| + \mathbb{E}(edh_\rightarrow(\mu))=$$
  Questo ci porta a concludere:
  $$edh_\rightarrow(a)[\geq 1 + \mathbb{E}] edh_\rightarrow(\mu)$$
  Il nostro esempio di funzione di rango probabilistica per $\rightarrow$ è
  quindi $edh_\rightarrow$.
\end{proof}
