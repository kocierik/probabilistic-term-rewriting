\chapter{Ordini probabilisticamente monotoni}

Al termine dello scorso capitolo, abbiamo presentato una tecnica corretta e
completa per provare che un PTRS sia SAST: esibire una funzione di rango
probabilistica per esso. In questo ultimo capitolo facciamo leva su questo
risultato per fornire una nuova tecnica di costruzione di PARS SAST a partire da
ARS che rispettano determinate condizioni.

\section{Motivazione}

Dato un generico PARS che vogliamo dimostrare essere SAST, la nostra prova
costruttiva del \cref{th:lyapunov-completeness} ci suggerirebbe di fare
affidamento sull'altezza attesa della derivazione. Dimostrare che essa
sia davvero una funzione di Lyapunov, però, non è sempre impresa facile nel caso
generale.

Questo problema ha una controparte non-probabilistica, da cui prenderemo
ispirazione: è la ricerca di ordini di semplificazione, e cioè ordini stretti di
riscrittura che soddisfano la seguente proprietà del sottotermine.

\begin{definition}[Proprietà del sottotermine]
  Sia $S \subseteq T^2(\Sigma, V)$. Si dice che $S$ gode della proprietà del
  sottotermine quando vale che
  $$\forall t \in T(\Sigma, V), p \in \mathcal{P}os(t) \setminus {\epsilon}
  \quad t S t|_p$$
\end{definition}

In presenza di effetti probabilistici, infatti, al posto della seguente nozione
di ordine di semplificazione abbiamo visto che è sufficiente applicare
$[\geq \epsilon + \mathbb{E}]$ alle rispettiva immagini di un termine e una
multidistribuzione attraverso la funzione di Lyapunov scelta.

\begin{definition}[Ordine di semplificazione]
  Sia $S \subseteq T^2(\Sigma, V)$. Si dice che $S$ è un ordine di
  semplificazione su $T(\Sigma, V)$ quando $S$ è un ordine stretto su
  $T(\Sigma, V)$ che gode della proprietà del sottotermine.
\end{definition}

Entrambi gli approcci forniscono insomma la garanzia di terminazione nei loro
rispettivi ambiti. Si potrebbe quindi pensare, sulla base di questa analogia, di
espandere le definizioni di specifici ordini di semplificazione al contesto
probabilistico, e adattare di conseguenza le tencniche per la loro costruzione.
Questa strategia ha già portato i suoi frutti: Avanzini et al.\ \cite{avanzini}
hanno effettuato questa operazione per il metodo di interpretazione, mostrandone
i risultati per gli ordini di semplificazione polinomiali e matriciali.

In questo capitolo tratteremo un nuovo strumento che segue questa tendenza: gli
ordini probabilisticamente monotoni.

\section{Definizione}

L'idea degli ordini probabilisticamente monotoni è quella di partire da un
ordine non probabilistico che a ogni passo di computazione deve essere emulato
con probabilità almeno $\epsilon$, con $\epsilon$ costante reale strettamente
positiva. Ecco gli accorgimenti che ci guidano verso la loro definizione:

\begin{itemize}
  \item imporremo che le multidisitribuzioni proprie di arrivo siano costruite a
    partire da insiemi finiti di indici. Questa non è un'assunzione forte:
    essendo i PTRS che vogliamo catturare tipicamente finiti, ed essendoci
    pertanto un numero finito di applicazioni di regola a partire da un dato
    termine, la seguente definizione fungerà comunque da ottimo prototipo di
    relazione di riscrittura probabilistica alla base di un possibile PTRS;
  \item in vista di una futura funzione di rango probabilistico, e cioè codifica
    verso $[\geq \epsilon + \mathbb{E}]$, vogliamo vincolare inferiormente la
    decrescita della lunghezza di derivazione attesa lungo un passo di
    riduzione. Per fare ciò, dovremo imporre valori minimi anche ad almeno una
    probabilità;
  \item la nostra definizione di (sotto)multidistribuzione tollera anche
    probabilità nulle. È desiderabile che la nostra costruzione si comporti
    indifferentemente fra due multidistribuzioni che differiscono solo per la
    presenza di esiti caratterizzati da tali probabilità degeneri.
\end{itemize}

\begin{definition}[Ordine probabilisticamente monotono]\label{def:pmo}
  Siano $\succ$ un sistema di riduzione astratto su $A$, $\epsilon, \delta \in
  \mathbb{R}_{> 0}$ e $f: A \rightarrow \mathbb{R}_{>0}$ una codifica di $\succ$
  in $[\geq \delta +]$. L'ordine probabilistico monotono (o PMO,
  \emph{probabilisticcally monotonic order}) su indotto da $\succ$ rispetto a
  $f$, $>_{p\succ}$, è il sistema di riduzione astratto probabilistico su $A$
  che ha per elementi tutte e sole le coppie della forma
  $$(a, \{\!\!\{ p_i : b_i \mathbin{|} i \in I\}\!\!\})$$
  (e cioè i cui secondi elementi sono sempre multinsiemi finiti da insiemi
  finiti di indici) che rispettano la seguente condizione:
  $$\forall i \in I \quad (p_i = 0 \vee f(a) \succeq f(b_i))$$
  $$\wedge$$
  $$\exists J \subseteq I \quad (\sum_{j \in J} p_j \geq \epsilon \quad \wedge
  \quad \forall j \in J \quad a \succ b_j)$$
\end{definition}

Il primo congiunto del precedente predicato esprime una proprietà che deve
essere condivisa da ogni elemento della multidistribuzione di arrivo: il
rispettare l'ordine non crescente da parte delle codifiche. Ovviamente, gli
esiti che si verificano con probabilità nulla sono esentati da questo vincolo.
Il secondo congiunto, invece, asserisce l'esistenza di un sottogruppo di esiti
che si faccia carico di ``diminuire'' il valore atteso durante un passo di
riduzione, esso potrebbbe essere composto perfino da un unico elemento o
dall'intero insieme $I$, ma di certo non può essere l'insieme vuoto, siccome
abbiamo scelto $\epsilon \in \mathbb{R}_{>0}$.

La seguente semplice implementazione in Standard ML fa leva sulla nozione che
nessuna probabilità assuma mai valori negativi. Se infatti esiste almeno un
$J \subseteq I$ che rispetti la condizione di cui sopra, allora essa sarà di
certo rispettata anche da $\{ j \in I \mathbin{|} a \succ b_j \}$ (\texttt{maxJ}
nel codice).

\smlrange{mpo.sml}{ordine probabilisticamente monotono}{5-14}

\section{Terminazione quasi certa forte}

Dimostrare la terminazione quasi certa forte degli ordini probabilisticamente
monotoni è, ovviamente, funzionale a provare che anche i PTRS che hanno un PMO
come relazione di riscrittura probabilistica siano SAST (per la
\cref{def:past-ptrs}). Perfino nel caso in cui la relazione di riscrittura
probabilistica in questione sia anche solo un sottoinsieme di un PMO si ha la
sicurezza che il PTRS in questione sia SAST: la funzione identità, infatti, è di
certo una codifica corretta dalla relazione di riscrittura verso il PMO.
Procediamo quindi a provare che ogni PMO indotto da ARS adeguati sia SAST. Per
farlo, naturalmente, useremo lo strumento delle funzioni di rango
probabilistiche, già introdotte nel precedente capitolo. Per aiutarci a traslare
i risultati di terminazione in presenza di effetti probabilistici, faremo uso
della seguente definizione ausiliaria, sulla base della quale costruiremo poi
l'unico lemma necessario alla dimostrazione.

\begin{definition}[Controparte probabilistica di un sistema di riduzione
  astratto]
  Sia $\rightarrow$ un sistema di riduzione astratto su $A$. Definiamo la sua
  controparte probabilistica, $\rightarrowtail$, come il seguente sistema di
  riduzione astratto probabilistico su $A$:

  $$\rightarrowtail := \{ (a, \mu) \in A \times \mathcal{M}(A) \mathbin{|}
  \exists b \in A \quad
  (\mu = \{\!\!\{ 1 : b \}\!\!\} \wedge a \rightarrow b)\}$$
\end{definition}

Il seguente lemma garantisce che le controparti probabilistiche degli ordini
di percorso lessicografici siano SAST se e solo se tali ordini abbiamo
solo altezze di derivazione finite. Ai fini della nostra esposizione, tali
controparti fungono quindi da ``nucleo'' di partenza per provare SAST su tutti
gli ordini probabilisticamente monotoni indotti da ARS con altezze di
derivazione esclusivamente finite.

\begin{lemma}[Funzioni di Lyapunov delle controparti probabilistiche dei
  sistemi di riduzione astratti]
  Sia $\rightarrow$ un sistema di riduzione astratto su $A$ e $\epsilon \in
  \mathbb{R}_{>0}$. Una generica funzione $f: A \rightarrow \mathbb{R}_{>0}$ è
  una codifica di $\rightarrow$ in $[\geq \epsilon +]$ se e solo se $f$ è una
  codifica della controparte probabilistica di $\rightarrow$, $\rightarrowtail$,
  in $[\geq \epsilon + \mathbb{E}]$.
\end{lemma}

\begin{proof}
  Siccome $f(a_i) = f(\{\!\!\{1: a_i\}\!\!\})$ e $f(\varnothing) = 0$ le due
  nozioni di monotonia diventano equivalenti.
\end{proof}

In realtà, la controparte probabilistica di un ARS è essa stessa un caso
degenere di ordine di ordine probabilisticamente monotono: basta fissare
$\epsilon = 1$ e, in tutte le coppie della relazione ottenuta, $J = I$. Partire
dal loro studio è quindi il passo più naturale verso il caso generale.

\begin{theorem}[Terminazione quasi certa forte degli ordini probabilisticamente
  monotoni]\label{th:main-theorem}
  Sia $\succ$ un sistema di riduzione astratto su $A$ e $\delta \in
  \mathbb{R}_{>0}$. L'ordine probabilisticamente monotono indotto da
  $\succ$ rispetto alla codifica $f: A \rightarrow \mathbb{R}_{>0}$ di $\succ$
  in $[\geq \delta +]$, $>_{p\succ}$, è quasi certamente terminante forte.
\end{theorem}

\begin{proof}
  Si consideri $\sqsupset$, la controparte probabilistica di $\succ$. Per il
  precedente lemma, $f$ codifica $\sqsupset$ in $[\geq \delta + \mathbb{E}]$.
  Sia inoltre $\epsilon \in \mathbb{R}_{>0}$ la costante usata per definire
  $>_{p\succ}$. Per chiusura di $\mathbb{R}_{>0}$ rispetto alla moltiplicazione,
  $\epsilon \delta \in \mathbb{R}_{>0}$. Quindi $[\geq \epsilon \delta +
  \mathbb{E}]$ rispetta il vincolo di positività stretta della relativa
  definizione. Consideriamo ora un generico elemento di $>_{p\succ}$, che
  indicheremo con $(a, \{\!\!\{p_i : b_i \mathbin{|} i \in I\}\!\!\})$ (per
  definizione di $>_{p\succ}$, la multidistribuzione ha necessariamente questa
  forma, e non abbiamo quindi perdita di generalità):
  \begin{enumerate}
    \item sia $i \in I$. Il primo congiunto della condizione espressa da
      $>_{p\succ}$ richiede che $p_i = 0 \vee f(a) \geq f(b_i)$. In entrambi i
      casi, osserviamo che quindi vale $p_i f(a) \geq p_i f(b_i)$ (siccome
      $p_i \geq 0$).
      % \begin{itemize}
      %   \item se $p_i = 0$, allora la disuguaglianza è banalmente soddisfatta;
      %   \item se $a \succeq b_i$, allora procediamo di nuovo per casi:
      %     \begin{itemize}
      %       \item se $a \succ b_i$, allora $a \sqsupset \{\!\!\{1:
      %         b_i\}\!\!\}$. Essendo $f$ una codifica di $\sqsupset$ in $[\geq
      %         \delta + \mathbb{E}]$, si ha che $f(a) \geq \delta +
      %         \mathbb{E}(f(\{\!\!\{1 : b_i\}\!\!\}))$ e perciò $f(a) \geq \delta
      %         + f(b_i)$. Essendo $\delta \in \mathbb{R}_{>0}$, di certo
      %         $f(a) \geq f(b_i)$. Infine, siccome $p_i \geq 0$, la
      %         disuguaglianza iniziale è verificata;
      %       \item se $a = b_i$, invece, la verifica è ovvia;
      %     \end{itemize}
      % \end{itemize}
    \item sia $J \subseteq I$ un insieme che soddisfa il secondo congiunto della
      condizione espressa da $>_{p\succ}$. Quindi $\sum_{j \in J} p_j \geq
      \epsilon \wedge \forall j \in J \quad a \succ b_j$. Fissiamo un generico
      indice $j \in J$. Da $a \succ b_j$ segue $a \sqsupset \{\!\!\{1 : b_j
      \}\!\!\}$. Sapendo che $f$ è una codifica di $\sqsupset$ in $[\geq
      \delta + \mathbb{E}]$, otteniamo $f(a) \geq \delta +
      \mathbb{E}(f(\{\!\!\{1 : b_j\}\!\!\}))$ e perciò $f(a) \geq \delta +
      f(b_j)$. Poiché $p_j > 0$, vale che $p_j f(a) \geq p_j \delta +
      p_j f(b_j)$. Per ogni possibile $j \in J$, sommiamo membro a membro le
      istanze di quest'ultima disuguaglianza e otteniamo $\sum_{j \in J} (p_j
      f(a)) \geq \sum_{j \in J} (p_j \delta) + \sum_{j \in J} (p_j f(b_j))$.
      Per il primo congiunto della condizione imposta da $>_{p\succ}$, $\sum_{j
      \in J} p_j \geq \epsilon$ e, siccome $\delta \in \mathbb{R}_{>0}$, allora
      $(\sum_{j \in J} p_j) \delta \geq \epsilon \delta$. Quindi $\sum_{j \in J}
      (p_j f(a)) \geq \epsilon \delta + \sum_{j \in J} (p_j f(b_j))$.
  \end{enumerate}
  Sommando membro a membro la disuguaglianza (2) con tutte le istanze della
  (1) per ogni $i \in I \setminus J$, si ottiene:
  $$\sum_{j \in J} (p_j f(a)) + \sum_{i \in I \setminus J}(p_i f(a)) \geq
  \epsilon \delta + \sum_{j \in J}(p_j f(b_j)) + \sum_{i \in I \setminus J}(p_i
  f(b_i))$$
  $$\sum_{i \in I}(p_i f(a)) \geq \epsilon \delta + \sum_{i \in I}(p_i f(b_i))$$
  $$f(a) \sum_{i \in I}p_i \geq \epsilon \delta + \sum_{i \in I}(p_i f(b_i))$$
  Ricordiamo che $>_{p\succ}$ è un PARS, e come tale i secondi membri dei suoi
  elementi sono sempre multidistribuzioni proprie ($\sum_{i \in I} p_i = 1$):
  $$f(a) \geq \epsilon \delta + \sum_{i \in I}(p_i f(b_i)) =
  \epsilon \delta + \mathbb{E}(f(\{\!\!\{p_i : b_i \mathbin{|} i \in I
  \}\!\!\}))$$
  Quindi $f$ è una codifica di $>_{p\succ}$ in $[\geq \epsilon \delta +
  \mathbb{E}]$, e cioè una funzione di Lyapunov. Perciò $>_{p\succ}$ è quasi
  certamente terminante forte.
\end{proof}

Proseguiamo la nostra trattazione con esempi di (non) applicazione di questo
teorema.

\section{Esempio di applicazione: ordini (giocattolo) booleani}

A titolo esemplificativo, mostriamo come già dei semplici ARS ``giocattolo'',
possano indurre degli ordini probabilisticamente monotoni tali da provare SAST
per programmi probabilistici reali, fra cui il seguente, che è ispirato a una
succesione geometrica.

\sml{geo.sml}{procedura \texttt{geo}}

La famiglia di ARS che definiremo a breve, definita ad hoc, sarà sufficiente:
essa si basa sul numero di occorrenze di simboli di funzione da un dato insieme
all'interno di un termine.

\begin{definition}[Numero di occorrenze in un termine]
  Siano $\Phi$ un insieme, $\Phi' \subseteq \Phi$, $\Sigma$ una segnatura su
  $\Phi$, $V$ un insieme di variabili e $s \in T(\Sigma, V)$. Il numero di
  occorrenze di simboli in $\Phi'$ all'interno di $s$, $|s|_{\Phi'}$ è definito
  come:

  $$|s|_{\Phi'} := \begin{cases}
    0 & s \in V \\
    \sum_{i = 1}^n |s_i|_{\Phi'} &
      s = f(s_1, \dots, s_n) \wedge f \notin \Phi'\\
    1 + \sum_{i = 1}^n |s_i|_{\Phi'} &
      s = f(s_1, \dots, s_n) \wedge f \in \Phi'\\
  \end{cases}$$
\end{definition}

Gli ordini booleani che andiamo a definire si basano sul semplice confronto di
questa quantità, che deve strettamente decrescere a ogni passo di riduzione.

\begin{definition}[Ordine booleano indotto da un insieme di simboli di funzione]
  Siano $\Phi$ un insieme, $\Phi' \subseteq \Phi$, $\Sigma$ una segnatura su
  $\Phi$, $V$ un insieme di variabili e $s \in T(\Sigma, V)$. L'ordine booleano
  indotto da $\Phi'$, $>_{\Phi'}$, è l'unico ARS su $T(\Sigma, V)$ che soddisfa
  la seguente condizione:
  $$\forall s, t \in T(\Sigma, V) \quad
  (s >_{\Phi'} t \leftrightarrow |s|_{\Phi'} > |t|_{\Phi'})$$
\end{definition}

Possiamo applicare il sistema di riduzione appena costruito a un semplice PTRS
per $\texttt{geo}$.

\begin{example}[Sistema di riscrittura dei termini probabilistico quasi
  certamente terminante forte per \texttt{geo}]
  Consideriamo $\Phi = \{0, S, \texttt{geo}\}$ (zero, funzione
  successore, procedura SML definita sopra). Ovviamente $\Sigma(0) = 0$,
  $\Sigma(S) = 1$ e $\Sigma(geo) = 1$. Il nostro PTRS $R$ avrà una sola
  regola, definito con l'ausilio della variabile $n$:

  $$geo(n) \quad R \quad
  \{\!\!\{\frac{1}{2}: geo(S(n)), \quad \frac{1}{2}: n\}\!\!\}$$

  Naturalmente, $|\cdot|_{\{geo\}}$ è una codifica di $>_{\{\Phi'\}}$ verso
  $[\geq 1 +]$. Fissato $\epsilon = \frac{1}{2}$, inoltre, possiamo costruire
  l'ordine probabilisticamente monotono indotto da $>_{\{\Phi'\}}$ rispetto a
  $|\cdot|_{\{Phi'\}}$, che sappiamo essere SAST per il \cref{th:main-theorem}.
  Quindi anche la relazione di riscrittura di $R$, che ne é sottoinsieme, è
  SAST. Quindi $R$ è SAST.
\end{example}

\section{Esempio di non-applicazione: ordini di percorso lessicografici}

L'unico vero limite della \cref{def:pmo} è la necessarietà di una codifica in
$[\geq \delta +]$. In questo paragrafo, esibiamo una famiglia di ordini di
semplificazione estremamente popolare (gli ordini di percorso lessicografici) e
proviamo in questo modo la non-applicabilità della tecnica degli ordini
probabilisticamente monotoni a titolo di esempio.

Gli ordini di percorso lessicografici appartengono alla famiglia degli ordini
di percorso ricorsivi, che confrontano dapprima i simboli alla radice dei due
termini in questione e poi, ricorsivamente, i loro sottotermini immediati. In
particolare, gli ordini di percorso lessicografici, come suggerisce il nome,
considerano i sottotermini immediati come tuple ordinate i cui i-esimi elementi
diventano rilevanti solo a parità di tutti quelli che li precedono. Se invece
avessimo scelto di prescindere dall'ordine dei sottotermini, avremmo potuto
effettuare uno studio degli ordini di percorso con multinsiemi.

\begin{definition}[Ordine di percorso lessicografico]
  Siano $\Phi$ un insieme finito di simboli di funzione, $\Sigma$ una segnatura
  per $\Phi$, $V$ un insieme di variabili e $>$ un ordine stretto su $\Sigma$.
  L'ordine di percorso lessicografico (o LPO, \emph{lexicographic path order})
  su $T(\Sigma, V)$ indotto da $>$, $>_{lpo}$, è la relazione su $T(\Sigma, V)$
  che rispetta la seguente condizione ricorsiva lessicografica su $(s, t)$:

  $\forall s,t \in T(\Sigma, V), s >_{lpo} t \iff$

  \textbf{(LPO1)} \quad $t \in \mathcal{V}ar(s) \quad \wedge \quad s \neq t \quad \vee$

  \textbf{(LPO2)} \quad $s = f(s_1, \dots, s_m) \quad \wedge \quad
                t = g(t_1, \dots, g_n) \quad \wedge$

  \textbf{(LPO2a)} \quad \quad $(\exists i \in \{1, \dots m\} \quad
                                s_i \geq_{lpo} t \quad \vee$

  \textbf{(LPO2b)} \quad \quad $f > g \quad \wedge \quad
                                \forall j \in \{1, \dots, n\} \quad
                                s >_{lpo} t_j \quad \vee$

  \textbf{(LPO2c)} \quad \quad $f = g \quad \wedge \quad
                                \forall j \in \{1, \dots, n\} \quad
                                s >_{lpo} t_j \quad \wedge$

  \quad \quad \quad \quad \quad \quad \quad $\exists i \in \{1, \dots, m\} \quad
                                (s_i >_{lpo} t_i \quad \wedge \quad
                                \forall k \in \{1, \dots, i - 1\} \quad
                                s_k = t_k))$
\end{definition}

Si osserva che:
\begin{itemize}
  \item $\geq_{lpo}$ è il quasi-ordine che consiste nella chiusura riflessiva di
  $>_{lpo}$: non potrebbe ovviamente essere l'ordine di percorso lessicografico
  indotto da $\geq$, che non è stretto;
  \item la ricorsione di cui sopra è ben definita, siccome usata solo su coppie
  di termini lessicograficamente più ``piccole'' di $(s, t)$.
\end{itemize}

Per formalizzare questo concetto, Baader e Nipkow \cite{baader} partono dal
presentare un'implementazione per un generico ordine lessicografico.

\smlrange{traat/orders.ML}{ordine lessicografico}{14-21}

Risulta necessaria anche la seguente definizione di \texttt{forall}.

\smlrange{traat/library.ML}{quantificatore universale per liste}{22-24}

L'ordine risultante è ovviamente definito sulle istanze di \texttt{term}, il
tipo precedentemente definito:

\smlrange{traat/termorders.ML}{ordine di percorso lessicografico}{17-31}

Provare la non-codificabilità in $[\geq \epsilon +]$ degli ordini di percorso
lessicografici è un problema risolvibile per più vie. Qui di seguito proponiamo
una possibile soluzione facente uso di assunzioni verificate da ogni LPO non
degenere.

\begin{theorem}[Non-codificabilità in $\geq \epsilon +$ degli ordini di percorso
  lessicografici]
  Siano $\Phi$ un insieme finito di simboli di funzione, $\Sigma$ una segnatura
  per $\Phi$, $V$ un insieme di variabili, $>$ un ordine stretto su $\Sigma$
  tale che $\exists f, g \in Phi \quad (f > g \wedge \Sigma(f) > 0)$ e $\epsilon
  \in \mathbb{R}_{>0}$. Non esistono codifiche $code:
  T(\Sigma, V) \rightarrow \mathbb{R}_{>0}$ di $>_{lpo}$, l'ordine di percorso
  lessicografico su $T(\Sigma, V)$, indotto da $>$ in $[\geq \epsilon +]$.
\end{theorem}
  
\begin{proof}
  Si considerino $t_0 = g(X, \dots, X)$ e $s = f(t_0, \dots, t_0)$. Partendo da
  $t_0$, costruiamo per induzione la succesione di termini tale che $\forall i
  \in \mathbb{N} \quad t_{i+1} = g(t_i, \dots, t_i)$. Osserviamo che:
  \begin{enumerate}
    \item per la clausola LPO2a, $\forall i \in \mathbb{N} \quad t_{i+1} >_{lpo}
      t_i$. Ne segue che $\forall k \in \mathbb{N} \quad code(t_{k}) \geq
      k \epsilon + code(t_0)$;
    \item per la clausola LPO2a, $s >_{lpo} t_0$. Per LPO2b e il principio di
      induzione, $\forall i \in \mathbb{N} \quad s >_{lpo} t_i$.
  \end{enumerate}
  Sia $n = \ceil[\bigg]{\frac{code(s) - code(t_0)}{\epsilon}}$. Per (2), di
  certo $n > 0$, e quindi $t_n$ è un termine della successione. Per (1), vale
  $$code(t_n) \geq n \epsilon + code(t_0)$$
  Quindi, siccome $code(s) \geq \epsilon + code(t_n)$, allora si ha che
  $$code(s) \geq (n + 1) \epsilon + code(t_0)$$
  Isolando $n$, otteniamo
  $$n \leq \frac{code(s) - code(t_0)}{\epsilon} - 1$$
  Per costruzione di $n$, assurdo.
\end{proof}

\section{Conclusioni}

Nel corso del nostro lavoro, abbiamo ripercorso le nozioni preliminari per
trattare i sistemi di riscrittura dei termini probabilistici. Abbiamo definito
gli ordini probabilisticamente monotoni, uno strumento per costruire sistemi di
riduzione astratti probabilistici a partire da sistemi di riduzione astratti
tradizionali ma codificabili in $[\geq \epsilon +]$. Abbiamo provato che tutti
gli ordini probabilisticamente monotoni sono quasi certamente terminanti forti,
con tanto di esempio applicativo. Infine, abbiamo anche portato un esempio
di ordine probabilisticamente monotono che invece non è costruibile: quello
indotto dagli ordini di percorso lessicografici.
