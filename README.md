# On Term Rewriting in the Presence of Probabilistic Effects

My thesis as a bachelor student in Computer Science at the University of
Bologna. Its main focus are probabilistic term rewriting and termination. My
supervisor was [Professor Dal
Lago](https://www.unibo.it/sitoweb/ugo.dallago/teachings?tab=tesi).

In this repository, you will find:

- the Standard ML programs (invoke them from the `src/` directory itself);
- the work's $\LaTeX$ source code;
- one more $\LaTeX$ document featuring some slides summing up the main topics.

If you are just interested in the resulting PDFs, you may take a look at
[my webpage](https://foxy.codeberg.page/writings).
