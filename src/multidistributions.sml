(* Assumendo di usare solo multinsiemi finiti da insiemi finiti di indici *)
type 'a multiset = 'a list;

(* Assumendo reali in [0, 1] e di somma <= 1 *)
type 'a multidistribution = (real * 'a) multiset;

(* real list -> real *)
fun sum l = foldr (fn (x, y) => x + y) 0 l

(* 'a multidistribution -> real *)
fun totalProbability mu = sum (map (fn (p, _) => p) mu)
