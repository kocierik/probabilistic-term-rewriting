use "traat/library.ML";
use "traat/termorders.ML";
use "multidistributions.sml";

(* term multidistribution -> term -> (term * term -> ord)
   -> term multidistribution *)
fun maxJ mu a ord = (filter (fn (_, bi) => ord (a, bi) = GR) mu)

(* (term * term -> order) -> term * term multidistribution -> real ->
   (term -> real) -> ord *)
fun mpo ord (a, mu) epsilon f =
  (forall (fn (pi, bi) => pi <= 0 orelse f a >= f bi) mu)
  andalso
  (totalProbability (maxJ mu a ord) >= epsilon)
