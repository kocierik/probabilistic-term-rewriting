(* Generatore pseudocasuale *)
val rand = Random.rand (0, 0);

(* unit -> bool *)
fun coinToss _ = Random.randRange (0, 1) rand = 1;

(* int -> int *)
fun geo n = if coinToss ()
  then geo (n + 1)
  else n;
